(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var experienceSchema = new Schema({
        nom: { type : String, required : true},
        debut: { type : String, required : true},
        fin: { type : String, required : true},
        entreprise: { type : String, required : true},
        mission: { type : String, required : true},
        description: { type : String, required : true},
        talent: { type : Schema.ObjectId, ref :'Talent', required : true}
    });
    

    module.exports = {
        experienceModel : mongoose.model('Experience', experienceSchema)
    }
})();
