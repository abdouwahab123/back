(function(){
    const Experience = require('./experience.model').experienceModel;
    const Talent = require('../Talent/talent.model').talentModel;
    module.exports = function(){
        return {
            list: function(req, res){
                Experience.find({}, (error, list)=>{
                    if (error){
                        res.status(500).json({
                            status: "error",
                            body: error 
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: list
                        })
                    }
                });
            },
            create: function(req, res){
                console.log('save', req.body);
                var experience = new Experience(req.body);
                experience.save((error, rep)=>{
                    if(error){
                        res.status(500).json({
                           status: "error",
                           body: error  
                        })
                    }else{
                        //recuperer dans le talent 
                        Talent.findOne({_id: req.body.talent}, function(err,talent){
                            if(talent){
                                talent.experiences.push(experience);
                                talent.save();
                            }
                        })
                        res.status(200).json({
                            status: "success",
                            body: rep
                        })
                    }
                })
            },
            read: function(req,res){
                Experience.findOne({_id: req.params.id}, function(error, experience){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!experience){
                        res.status(404).json({
                            status: "not found",
                            body: experience
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: experience
                        })
                    }
                })
            },
            update: function(req,res){
                Experience.findOneAndUpdate({_id: req.params.id},req.body, {new: true} ,function(error, experience){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: experience
                        })
                    }
                })
            },
            delete: function (req,res) {
                Experience.deleteOne({_id: req.params.id}, function(error, rep){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(201).end()
                    }
                })
            }
        }
    }

})();