(function(){
    module.exports = function(app){
        const Ctrl = require('./experiences.controller')();
        app.route('/experience')
            .get(Ctrl.list)
            .post(Ctrl.create)
        app.route('/experience/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)            
    }
})();