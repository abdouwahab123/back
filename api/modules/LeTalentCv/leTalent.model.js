(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var cvSchema = new Schema({
        fichier: { type : String, required : true},
        talent: { type : Schema.ObjectId, ref :'Talent', required : true},
        extension: { type : String, required : false} 
    });

    module.exports = {
        cvModel : mongoose.model('CV', cvSchema)
    }
})();
