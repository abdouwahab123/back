(function(){
    module.exports = function(app){
        const Ctrl = require('./leTalent.controller')();
        app.route('/cva')
            .get(Ctrl.list)
            .post(Ctrl.upload)
        app.route('/cva/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)            
    }
})();