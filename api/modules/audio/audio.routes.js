(function(){
    module.exports = function(app){
        const Ctrl = require('./audio.controller')();
        app.route('/audio')
            .get(Ctrl.list)
            .post(Ctrl.upload)
        app.route('/audio/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)            
    }
})();