(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var musiqueSchema = new Schema({
        nom: { type : String, required : true},
        sound: { type : String, required : true},
        talent: { type : Schema.ObjectId, ref :'Talent', required : true},
        date: { type : Date, required : false},
        extension: { type : String, required : false}
    });

    module.exports = {
        musiqueModel : mongoose.model('Musique', musiqueSchema)
    }
})();
