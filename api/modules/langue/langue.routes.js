(function(){
    module.exports = function(app){
        const Ctrl = require('./langue.controller')();
        app.route('/langue')
            .get(Ctrl.list)
            .post(Ctrl.create)
        app.route('/langue/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)            
    }
})();