(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var langueSchema = new Schema({
        nom: { type : String, required : true},
        niveau: { type : String, required : true},
        talent: { type : Schema.ObjectId, ref :'Talent', required : true}
    });
    

    module.exports = {
        langueModel : mongoose.model('Langue', langueSchema)
    }
})();
