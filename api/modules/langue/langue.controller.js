(function(){
    const Langue = require('./langue.model').langueModel;
    const Talent = require('../Talent/talent.model').talentModel;
    module.exports = function(){
        return {
            list: function(req, res){
                Langue.find({}, (error, list)=>{
                    if (error){
                        res.status(500).json({
                            status: "error",
                            body: error 
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: list
                        })
                    }
                });
            },
            create: function(req, res){
                console.log('save', req.body);
                var langue = new Langue(req.body);
                langue.save((error, rep)=>{
                    if(error){
                        res.status(500).json({
                           status: "error",
                           body: error  
                        })
                    }else{
                        Talent.findOne({_id: req.body.talent}, function(err,talent){
                            if(talent){
                                talent.langues.push(langue);
                                talent.save();
                            }
                        })
                        res.status(200).json({
                            status: "success",
                            body: rep
                        })
                    }
                })
            },
            read: function(req,res){
                Langue.findOne({_id: req.params.id}, function(error, langue){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!langue){
                        res.status(404).json({
                            status: "not found",
                            body: langue
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: langue
                        })
                    }
                })
            },
            update: function(req,res){
                Langue.findOneAndUpdate({_id: req.params.id},req.body, {new: true} ,function(error, langue){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: formation
                        })
                    }
                })
            },
            delete: function (req,res) {
                Langue.deleteOne({_id: req.params.id}, function(error, rep){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(201).end()
                    }
                })
            }
        }
    }

})();