(function(){
    const Talent = require('../Talent/talent.model').talentModel;
    module.exports = function(){
        return {
           
            login: function(req,res){
                Talent.findOne({email: req.body.email, password: req.body.password}, function(error, talent){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!talent){
                        res.status(404).json({
                            status: "not found",
                            body: talent
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: talent
                        })
                    }
                }).populate('langues').populate('competences').populate('formations').populate('experiences')
            },
            login1: function(req,res){
                Talent.findOne({_id: req.body._id}, function(error, talent){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!talent){
                        res.status(404).json({
                            status: "not found",
                            body: talent
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: talent
                        })
                    }
                }).populate('langues').populate('competences').populate('formations').populate('experiences')
            }
        }
    }
})();