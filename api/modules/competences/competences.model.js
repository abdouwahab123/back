(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var competenceSchema = new Schema({
        nom: { type : String, required : true},
        niveau: { type : String, required : true},
        type: { type : String, required : true},
        talent: { type : Schema.ObjectId, ref :'Talent', required : true}       

    });

    module.exports = {
        competenceModel : mongoose.model('Competence', competenceSchema)
    }
})();
