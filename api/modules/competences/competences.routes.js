(function(){
    module.exports = function(app){
        const Ctrl = require('./competences.controller')();
        app.route('/competence')
            .get(Ctrl.list)
            .post(Ctrl.create)
        app.route('/competence/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)            
    }
})();