(function(){
    const Competence = require('./competences.model').competenceModel;
    const Talent = require('../Talent/talent.model').talentModel;
    module.exports = function(){
        return {
            list: function(req, res){
                Competence.find({}, (error, list)=>{
                    if (error){
                        res.status(500).json({
                            status: "error",
                            body: error 
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: list
                        })
                    }
                });
            },
            create: function(req, res){
                console.log('save', req.body);
                var competence = new Competence(req.body);
                competence.save((error, rep)=>{
                    if(error){
                        res.status(500).json({
                           status: "error",
                           body: error  
                        })
                    }else{
                        Talent.findOne({_id: req.body.talent}, function(err,talent){
                            if(talent){
                                talent.competences.push(competence);
                                talent.save();
                            }
                        })
                        res.status(200).json({
                            status: "success",
                            body: rep
                        })
                    }
                })
            },
            read: function(req,res){
                Competence.findOne({_id: req.params.id}, function(error, competence){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!user){
                        res.status(404).json({
                            status: "not found",
                            body: competence
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: user
                        })
                    }
                })
            },
            update: function(req,res){
                Competence.findOneAndUpdate({_id: req.params.id},req.body, {new: true} ,function(error, competence){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: competence
                        })
                    }
                })
            },
            delete: function (req,res) {
                Competence.deleteOne({_id: req.params.id}, function(error, rep){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(201).end()
                    }
                })
            }
        }
    }

})();