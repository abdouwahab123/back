(function(){
    module.exports = function(app){
        const Ctrl = require('./talent.controller')();
        app.route('/talent')
            .get(Ctrl.list)
            .post(Ctrl.create)
        app.route('/talent/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)            
    }
})();