(function(){
    const Talent = require('./talent.model').talentModel;
    module.exports = function(){
        return {
            list: function(req, res){
                Talent.find({}, (error, list)=>{
                    if (error){
                        res.status(500).json({
                            status: "error",
                            body: error 
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: list
                        })
                    }
                }).populate('langues').populate('competences').populate('formations').populate('experiences');
            },
            create: function(req, res){
                console.log('save', req.body);
                var talent = new Talent(req.body);
                talent.save((error, rep)=>{
                    if(error){
                        res.status(500).json({
                           status: "error",
                           body: error  
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: rep
                        })
                    }
                })
            },
            read: function(req,res){
                Talent.findOne({_id: req.params.id}, function(error, talent){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!talent){
                        res.status(404).json({
                            status: "not found",
                            body: talent
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: talent
                        })
                    }
                }).populate('competences').populate('formations').populate('langues').populate('experiences')
            },
            update: function(req,res){
                Talent.findOneAndUpdate({_id: req.params.id},req.body, {new: true} ,function(error, talent){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: talent
                        })
                    }
                })
            },
            delete: function (req,res) {
                Talent.deleteOne({_id: req.params.id}, function(error, rep){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(201).end()
                    }
                })
            }
        }
    }

})();