(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var talentSchema = new Schema({
        nom: { type : String, required : true},
        email: { type : String, required : true},
        password: { type : String, required : true},
        cohort: { type : String, required : true},
        programme: { type : String, required : true},
        linkedin: { type : String, required : true},
        github: { type : String, required : true},
        numero: { type : String, required : true},
        competences: [{ type : Schema.ObjectId, ref :'Competence'}],
        langues: [{ type : Schema.ObjectId, ref :'Langue'}],
        formations: [{ type : Schema.ObjectId, ref :'Formation'}],
        experiences: [{ type : Schema.ObjectId, ref :'Experience'}],
        toPublish: { type : String, required : false}
    });

    module.exports = {
        talentModel : mongoose.model('Talent', talentSchema)
    }
})();
