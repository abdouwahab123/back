(function(){
    module.exports = function(app){
        const Ctrl = require('./formation.controller')();
        app.route('/formation')
            .get(Ctrl.list)
            .post(Ctrl.create)
        app.route('/formation/:id')
            .get(Ctrl.read)
            .put(Ctrl.update)
            .delete(Ctrl.delete)            
    }
})();