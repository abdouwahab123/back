(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var formationSchema = new Schema({
        nom: { type : String, required : true},
        lieu: { type : String, required : true},
        date: { type : String, required : true},
        talent: { type : Schema.ObjectId, ref :'Talent', required : true}
    });
    

    module.exports = {
        formationModel : mongoose.model('Formation', formationSchema)
    }
})();
