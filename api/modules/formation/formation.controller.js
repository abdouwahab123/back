(function(){
    const Formation = require('./formation.model').formationModel;
    const Talent = require('../Talent/talent.model').talentModel;
    module.exports = function(){
        return {
            list: function(req, res){
                Formation.find({}, (error, list)=>{
                    if (error){
                        res.status(500).json({
                            status: "error",
                            body: error 
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: list
                        })
                    }
                });
            },
            create: function(req, res){
                console.log('save', req.body);
                var formation = new Formation(req.body);
                formation.save((error, rep)=>{
                    if(error){
                        res.status(500).json({
                           status: "error",
                           body: error  
                        })
                    }else{
                        Talent.findOne({_id: req.body.talent}, function(err,talent){
                            if(talent){
                                talent.formations.push(formation);
                                talent.save();
                            }
                        })
                        res.status(200).json({
                            status: "success",
                            body: rep
                        })
                    }
                })
            },
            read: function(req,res){
                Formation.findOne({_id: req.params.id}, function(error, experience){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else if(!formation){
                        res.status(404).json({
                            status: "not found",
                            body: formation
                        })
                    }
                    else{
                        res.status(200).json({
                            status: "success",
                            body: formation
                        })
                    }
                })
            },
            update: function(req,res){
                Formation.findOneAndUpdate({_id: req.params.id},req.body, {new: true} ,function(error, formation){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(200).json({
                            status: "success",
                            body: formation
                        })
                    }
                })
            },
            delete: function (req,res) {
                Formation.deleteOne({_id: req.params.id}, function(error, rep){
                    if(error){
                        res.status(500).json({
                            status: "error",
                            body: error
                        })
                    }else{
                        res.status(201).end()
                    }
                })
            }
        }
    }

})();