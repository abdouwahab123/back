const express = require('express'),
app = express(),
bodyParser = require('body-parser'),
mongoose = require('mongoose');
const nodemailer = require("nodemailer");
let path= require('path');
let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: "leslielacombe12345@gmail.com", // generated ethereal user
      pass: "Leslie12345"// generated ethereal password
    }
});
//Files = require('./api/utils/Files)
require('dotenv').config();
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json())

mongoose.connection.openUri(process.env.MONGO_URL, {}, err =>{
    if (err){
        console.log('MongoDB connection error', err);
        process.exit(1);
    }
    initApp();
})

//LOAD ROUTES
function initApp(){
    app.use(function (req, res, next){ 
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'token, Content-Type, X-requested-With');
        res.setHeader('Access-Control-Allow-Credentials', true);
        if(req.method == 'OPTIONS') return res.sendStatus(200);
        next();
    });
    app.use(express.static(path.join(__dirname, 'public')));

    require('./api/modules/Talent/talent.routes')(app)
    require('./api/modules/sendMail/sendMail.routes')(app)
    require('./api/modules/login/login.routes')(app)
    require('./api/modules/leTalentCv/leTalent.routes')(app)
    require('./api/modules/audio/audio.routes')(app)
    require('./api/modules/competences/competences.routes')(app)
    require('./api/modules/formation/formation.routes')(app)
    require('./api/modules/langue/langue.routes')(app)
    require('./api/modules/experiences/experiences.routes')(app)

    app.use(function(req, res){
        res.status(404).send('OUPS PAGE INTROUVABLE')
    })

    var server = app.listen(process.env.NODE_PORT, function(){
        console.log("L'application tourne sur le port ", server.address().port)
    })
};